RTags for Debian
----------------

This package includes the `rc` and `rdm` binaries renamed to
`rtags-rc` and `rtags-rdm` correspondingly to avoid collision with
already existing files from other debian packages.

In order to create a complete index, rtags may need additional include
path to find `stddef.h` or `stdarg.h` or similar files.  One way to
achieve that is to install recommended package
`libclang-common-*-dev`. Alternatively you can specify the path to the
files in `$XDG_CONFIG_HOME/rtags/rdmrc` configuration file. For
example in case you have installed package `libgcc-8-dev`, add line

    --include-path /usr/lib/gcc/x86_64-linux-gnu/8/include/

to the `~/.config/rtags/rdmrc` file.

For users of `systemd` service manager, `rtags` package ships
user-mode unit files, where `rdm.service` service is socket activated.
You can check its current status by running the following commands:

    systemctl --user status rdm.socket
    systemctl --user status rdm.service

Emacs with `elpa-rtags` package can automatically start the indexer
process unless the process is already running with the function
`(rtags-start-process-unless-running)` for example added to a hook

    (add-hook 'c-mode-hook 'rtags-start-process-unless-running)
    (add-hook 'c++-mode-hook 'rtags-start-process-unless-running)

To manually start the `rtags-rdm` indexer, run `rtags-rdm` in a shell.
