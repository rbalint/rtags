Source: rtags
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Denis Danilov <danilovdenis@yandex.ru>
Section: devel
Priority: optional
Build-Depends: debhelper-compat (=12),
 bash-completion,
 cmake,
 dh-elpa,
 libclang-8-dev,
 llvm-8,
 pkg-config,
 zlib1g-dev
Standards-Version: 4.4.1
Homepage: https://github.com/Andersbakken/rtags
Vcs-Browser: https://salsa.debian.org/emacsen-team/rtags
Vcs-Git: https://salsa.debian.org/emacsen-team/rtags.git

Package: rtags
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: libclang-common-8-dev, elpa-rtags
Description: C/C++ client/server indexer with integration for Emacs
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc. There’s also limited support for
 ObjC/ObjC++. It allows you to find symbols by name (including nested
 class and namespace scope). Most importantly it gives you proper
 follow-symbol and find-references support. It also have neat little
 things like rename-symbol, integration with clang’s “fixits”
 (https://clang.llvm.org/diagnostics.html).

Package: elpa-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends},
 rtags
Recommends: emacs (>= 46.0)
Suggests: elpa-ac-rtags,
 elpa-company-rtags,
 elpa-flycheck-rtags,
 elpa-helm-rtags,
 elpa-ivy-rtags
Enhances: emacs, rtags
Description: emacs front-end for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the minor mode for RTags.

Package: elpa-company-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs, elpa-rtags
Description: company back-end for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the company completion back-end.

Package: elpa-flycheck-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs, elpa-rtags
Description: flycheck integration for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the flycheck integration.

Package: elpa-ac-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs, elpa-rtags
Description: auto-complete back-end for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the auto-complete back-end.

Package: elpa-ivy-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: ivy back-end for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the ivy back-end.

Package: elpa-helm-rtags
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs, elpa-rtags
Description: helm interface for RTags
 RTags is a client/server application that indexes C/C++ code and
 keeps a persistent file-based database of references, declarations,
 definitions, symbolnames etc.
 .
 This package provides the helm interface.
